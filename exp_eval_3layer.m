function exp_mat = exp_eval_3layer(y, h_arr, alpha_arr, finite)
%exp_mat = exp_eval_3layer(y, h_arr, alpha_arr, finite)
%
%Function to load the exponential terms appearing in the three-layer
%Laplace-transformed Green's functions expressions. These are evaluated in
%functional form to allow for any selection of inversion contour.
%
%Inputs: y         - location of interrogation point within solid [1 x 1]
%        h_arr     - thickness of paint/insulator layers [N x 1]
%        alpha_arr - thermal diffusivity of each layer [N x 1]
%        finite    - binary switch to evaluate finite (vs. semi-infinite) exponentials  [1 x 1]
%
%Outputs: exp_mat  - array of exponential functions [N^2+N x 1]
%
%
%Whalen, Laurence, Marineau, and Ozawa: "A Green’s Function Approach to
%Heat-Flux Estimation from Temperature-Sensitive Paint Measurements"
%
%Please send all comments to whalen@umd.edu
%



%unpack
y1 = h_arr(1);
y2 = h_arr(2);
y3 = h_arr(3);
alpha1 = alpha_arr(1);
alpha2 = alpha_arr(2);
alpha3 = alpha_arr(3);

y1 = -abs(y1);
y2 = -abs(y2);
y3 = -abs(y3);


a1 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y1)/sqrt(alpha1)) );
a2 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y1)/sqrt(alpha2)) );
a3 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y1)/sqrt(alpha3)) );
b1 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y2)/sqrt(alpha1)) );
b2 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y2)/sqrt(alpha2)) );
b3 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y2)/sqrt(alpha3)) );
z1 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y )/sqrt(alpha1)) );
z2 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y )/sqrt(alpha2)) );
z3 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y )/sqrt(alpha3)) );
switch finite
    case 0
        c1 = @(r, theta)   0;
        c2 = @(r, theta)   0;
        c3 = @(r, theta)   0;
        
    case 1
        c1 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y3)/sqrt(alpha1)) );
        c2 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y3)/sqrt(alpha2)) );
        c3 = @(r, theta)   ( exp(sqrt(r*exp(1i*theta))*(y3)/sqrt(alpha3)) );

end



exp_mat = {a1; 
           a2; 
           a3; 
           b1; 
           b2; 
           b3; 
           c1; 
           c2; 
           c3; 
           z1; 
           z2; 
           z3};


return;


end

