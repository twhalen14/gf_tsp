function q_mat = irt_heat_flux(T_mat, t_arr, h_arr, alpha_arr, k_arr)
%tsp_heat_flux(T_mat, t_arr, h_arr, alpha_arr, k_arr)
%
%Function to compute the heat flux based on IR thermograpy measurements. Uses a
%Green's function to describe the evolution of the temperature profile on
%the painted model's surface and calls the Regularization Tools package of
%P.C. Hansen to solve the noisy discrete problem. 
%
%Inputs:
%       T_mat     - 3D matrix of temperature histories [NROW x NCOL x M]
%       t_arr     - equally spaced data acquisition times [1 x M]
%       h_arr     - thickness of paint/insulator layers [N x 1]
%       alpha_arr - thermal diffusivities [N x 1]
%       k_arr     - thermal conductivities [N x 1]
%
%Outputs: gf_mat   - 3D matrix of heat-flux histories [NROW x NCOL x M]
%
%
%Whalen, Laurence, Marineau, and Ozawa: "A Green’s Function Approach to
%Heat-Flux Estimation from Temperature-Sensitive Paint Measurements"
%
%Please send all comments to whalen@umd.edu
%


%add path to regularization tools
addpath reg_tools


n_img = size(T_mat,3);
n_row = size(T_mat,1);
n_col = size(T_mat,2);
dt = diff(t_arr(1:2));


%%%%%%%%%%%%
% COMPUTE THE TSP GF

fprintf('\nCalculating TSP Green''s function:\n');
t_tau_arr = t_arr - t_arr(1);
gf_tsp = internal_gf_invert(h_arr, alpha_arr, k_arr, t_tau_arr, 0);
gf_tsp_mat = alpha_arr(1)/k_arr(1) * toeplitz(dt*real(gf_tsp));
gf_tsp_mat = gf_tsp_mat - triu(gf_tsp_mat, 1);




%%%%%%%%%%%%
% CALCULATE HEAT FLUX FROM IRT TEMPERATURE

fprintf('\nReconstructing heat flux:\n');
fprintf('                ');
svd_lim = 2500;
sub_num = 1000;
lambda_scale = 4;
if n_img > svd_lim
    [U_tsp, S_tsp, V_tsp] = svd(gf_tsp_mat(1:sub_num,1:sub_num));
    Vsp = dct(eye(size(gf_tsp_mat,1)));
    subspace = 50;
else
    [U_tsp, S_tsp, V_tsp] = svd(gf_tsp_mat);
end

q_mat = zeros(n_row, n_col, n_img);
for i = 1:n_row
    
    fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bRow: %05d/%05d', i, n_row);

    for j = 1:n_col
        
        
        %process current pixel
        T_curr = squeeze(T_mat(i,j,:));
        T_curr = movmean(movmean(T_curr, 5), 3);  %clean up with limited temporal smoothing
        T_curr = T_curr - T_curr(1);

        %if problem is too large for svd, use iterative routine
        if n_img > svd_lim
            
            %solve the 1000-point subproblem for lambda
            [~, rho, eta, reg_param] = l_curve(U_tsp, diag(S_tsp), T_curr, 'Tikh');
            lambda = l_corner(rho, eta, reg_param);
            
            %iterative preconditioned least-squares with scaled lambda
            q_curr = splsqr(gf_tsp_mat, T_curr, lambda_scale*lambda, Vsp(:,1:subspace));
            q_mat(i,j,:) = q_curr(:,end);
            
        else
            
            %svd-based tikhnonov regularization
            [~, rho, eta, reg_param] = l_curve(U_tsp, diag(S_tsp), T_curr, 'Tikh');
            lambda = l_corner(rho, eta, reg_param);
            q_mat(i,j,:) = tikhonov(U_tsp, diag(S_tsp), V_tsp, T_curr, lambda_scale*lambda, ones(n_img,1));

        end
    end
    
end
fprintf('\n\n');




return;




end

