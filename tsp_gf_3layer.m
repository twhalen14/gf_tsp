function gf_mat = tsp_gf_3layer(h1, alpha_arr, k_arr, exp_mat, r0, theta_bc, theta_cb_prime, theta_vec, r_vec)
%gf_mat = tsp_gf_3layer(h1, alpha_arr, k_arr, exp_mat, r0, theta_bc, theta_cb_prime, theta_vec, r_vec)
%
%Functional to load the TSP Green's function for a three-layer composite
%solid. Expressions returned in function form with inverse Laplace
%generating function for later contour integration.
%
%Inputs: h1        - thickness of TSP layer [1 x 1]
%        alpha_arr - thermal diffusivity of each layer [N x 1]
%        k_arr     - thermal conductivity of each layer  [N x 1]
%        exp_mat   - matrix of exponential functions evaluated along each contour branch [1 x 3 CELL]
%        r0        - radius of inner contour arc [1 x 1]
%        theta_bc  - angle of BC contour branch [1 x 1]
%        theta_cb_prime  - angle of C'B' contour branch [1 x 1]
%        theta_vec - array of theta values for CC' branch [25000 x 1]
%        r_vec     - array of radius values for BC and C'B' branches [25000 x 1]
%
%Outputs: gf_mat   - array of GF functions [N x 3]
%
%
%Whalen, Laurence, Marineau, and Ozawa: "A Green’s Function Approach to
%Heat-Flux Estimation from Temperature-Sensitive Paint Measurements"
%
%Please send all comments to whalen@umd.edu
%

%transpose these
r_vec = r_vec';
theta_vec = theta_vec';

%unpack
alpha1 = alpha_arr(1);
alpha2 = alpha_arr(2);
alpha3 = alpha_arr(3);
k1 = k_arr(1);
k2 = k_arr(2);
k3 = k_arr(3);


%system constants
xi12 = (k1*sqrt(alpha2) - k2*sqrt(alpha1))/(k1*sqrt(alpha2) + k2*sqrt(alpha1));
xi23 = (k2*sqrt(alpha3) - k3*sqrt(alpha2))/(k2*sqrt(alpha3) + k3*sqrt(alpha2));


%get green's function handles -- currently not evaluating mean GFs for lower two layers      
f11 = @(t_tau) gf_tsp_func(h1, r_vec ,       theta_bc, t_tau, exp_mat{1}, xi12, xi23);
f12 = @(t_tau) gf_tsp_func(h1, r0,          theta_vec, t_tau, exp_mat{2}, xi12, xi23);
f13 = @(t_tau) gf_tsp_func(h1, r_vec , theta_cb_prime, t_tau, exp_mat{3}, xi12, xi23);             
f21 = @(t_tau) 0;
f22 = @(t_tau) 0;
f23 = @(t_tau) 0;
f31 = @(t_tau) 0;
f32 = @(t_tau) 0;
f33 = @(t_tau) 0;


%return this in matrix form, [N x 3] (for each portion of the integral contour)
gf_mat = {f11, f12, f13;
          f21, f22, f23;
          f31, f32, f33};


return;



    function f = gf_tsp_func(h1, r, theta, t_tau, exp_mat, xi12, xi23)
            
        a1 = exp_mat(:,1);
        a2 = exp_mat(:,2);
        b2 = exp_mat(:,5);
        b3 = exp_mat(:,6);
        c3 = exp_mat(:,9);
            
        %if r is a scalar, we are on the cc' integration path, so include
        %extra factor from integrand change-of-variable
        if numel(r) == 1
            f = ( (...
                1./(r*exp(1i*theta)) - ...
            (a1 .* (1-xi12) .* (...
                   (    1 .* a2.^2 .* b3.^2 -     1 .* b2.^2 .* c3.^2) +   ...
             xi23.*(    1 .* a2.^2 .* c3.^2 -     1 .* b2.^2 .* b3.^2)))./  ...
           ((       (   1 .* a2.^2 .* b3.^2 - a1.^2 .* b2.^2 .* c3.^2) +   ...
              xi12.*(   1 .* b2.^2 .* c3.^2 - a1.^2 .* a2.^2 .* b3.^2) +   ... 
              xi23.*(   1 .* a2.^2 .* c3.^2 - a1.^2 .* b2.^2 .* b3.^2) +   ...
        xi12.*xi23.*(   1 .* b2.^2 .* b3.^2 - a1.^2 .* a2.^2 .* c3.^2)    ... 
              ).* ...
                   (r*exp(1i*theta)) ) ) .* ...
              exp(r*exp(1i*theta)*t_tau) .* exp(1i*theta) * 1i * r * 1/h1);
        else
            f = ( (...
                1./(r*exp(1i*theta)) - ...
            (a1 .* (1-xi12) .* (...
                   (    1 .* a2.^2 .* b3.^2 -     1 .* b2.^2 .* c3.^2) +   ...
             xi23.*(    1 .* a2.^2 .* c3.^2 -     1 .* b2.^2 .* b3.^2)))./  ...
           ((       (   1 .* a2.^2 .* b3.^2 - a1.^2 .* b2.^2 .* c3.^2) +   ...
              xi12.*(   1 .* b2.^2 .* c3.^2 - a1.^2 .* a2.^2 .* b3.^2) +   ... 
              xi23.*(   1 .* a2.^2 .* c3.^2 - a1.^2 .* b2.^2 .* b3.^2) +   ...
        xi12.*xi23.*(   1 .* b2.^2 .* b3.^2 - a1.^2 .* a2.^2 .* c3.^2)    ... 
              ).* ...
                   (r*exp(1i*theta)) ) ) .* ...
              exp(r*exp(1i*theta)*t_tau) .* exp(1i*theta) * 1/h1 );
        end
    end
   
            
end

