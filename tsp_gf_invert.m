function gf = tsp_gf_invert(h_arr, alpha_arr, k_arr, t_tau_arr)
%gf = tsp_gf_invert(h_arr, alpha_arr, k_arr, t_tau_arr)
%
%Function to obtain a TSP Green's function on a two- or three-layer solid
%of finite or semi-infinite extent via contour integration of the
%Laplace-transformed expressions.
%
%Inputs: h_arr     - thickness of paint/insulator layers [N x 1]
%        alpha_arr - thermal diffusivity of each layer  [N x 1]
%        k_arr     - thermal conductivity of each layer  [N x 1]
%        t_tau     - array of convolution 'lag' values [1 x M]
%
%Outputs: gf       - array of GF values [M x 1]
%
%Note that the dimension of 'h_arr' determines the number of layers. Also,
%if the final entry of 'h_arr' is INF, will evaluate the semi-infinite
%versions of the Green's functions.
%
%
%Whalen, Laurence, Marineau, and Ozawa: "A Green’s Function Approach to
%Heat-Flux Estimation from Temperature-Sensitive Paint Measurements"
%
%Please send all comments to whalen@umd.edu
%


%since h_arr is thickness array, convert to location within solid
h_arr = cumsum(abs(h_arr));

h1 = abs(h_arr(1));  
h_arr = -abs(h_arr);   %ensure these are negative values
n_layers = numel(h_arr);
if isinf(h_arr(end)) 
    finite = 0;
else
    finite = 1;
end


%time-step size
if size(t_tau_arr,1) > 1
    t_tau_arr = t_tau_arr';
end
dt = t_tau_arr(2) - t_tau_arr(1);
t_tau_arr = [t_tau_arr, t_tau_arr(end)+dt];


%choose R, r0 and delta values for integration
r0 = 0.25;        %assumed values, chosen through trial and error
Rlog = 12;
delta = 0.1;
theta_bc = pi-(delta*pi/180);
theta_cb_prime = -pi+(delta*pi/180);
n_point_int = 25000;
theta_vec = linspace(theta_bc, theta_cb_prime, n_point_int);
r_vec = logspace(log10(r0), Rlog, n_point_int);




%get the appropriate EXP functions for use in the GFs
switch n_layers
    case 1
        error('Single layer Green''s function can be inverted analytically and is not currently available in this package.');
    case 2
        exp_mat = exp_eval_2layer(0, h_arr, alpha_arr, finite);
    case 3
        exp_mat = exp_eval_3layer(0, h_arr, alpha_arr, finite);
end



%evaluate EXP functions from geometry and constant integration contour
n_exp = numel(exp_mat);
exp_bc_mat       = zeros(n_point_int, n_exp);
exp_cc_prime_mat = zeros(n_point_int, n_exp);
exp_cb_prime_mat = zeros(n_point_int, n_exp);
for i = 1:n_exp
    exp_bc_mat(:,i)       = exp_mat{i}(r_vec, theta_bc);
    exp_cc_prime_mat(:,i) = exp_mat{i}(r0   , theta_vec);
    exp_cb_prime_mat(:,i) = exp_mat{i}(r_vec, theta_cb_prime);
end
exp_mat = {exp_bc_mat; exp_cc_prime_mat; exp_cb_prime_mat};



%finally, get the GFs -- the only variable remaining should be the lag 't_tau'
switch n_layers
    case 1
        error('Single layer Green''s function can be inverted analytically and is not currently available in this package.');
    case 2
        gf_mat  = tsp_gf_2layer(h1, alpha_arr, k_arr, exp_mat, r0, theta_bc, theta_cb_prime, theta_vec, r_vec);
    case 3
        gf_mat  = tsp_gf_3layer(h1, alpha_arr, k_arr, exp_mat, r0, theta_bc, theta_cb_prime, theta_vec, r_vec);
end

  
      
             
%compute the TSP GF for all lag values
gf = zeros(numel(t_tau_arr)-1,1);
weights = [ 0.236927, 0.478629, 0.568889, 0.478629, 0.236927];
points  = [-0.906180,-0.538469, 0.000000, 0.538469, 0.906180];
fprintf('\n          ');
for i = 1:numel(t_tau_arr)-1
    
    l_lim = t_tau_arr(i);
    u_lim = t_tau_arr(i+1);
    t_tau_quad_arr = (u_lim-l_lim)/2 * points + (u_lim+l_lim)/2;
    
    %perform the quadrature
    gf_aux_quad = laplace_invert(t_tau_quad_arr, gf_mat, theta_vec, r_vec);
    gf(i,1) = 1/2 * sum(weights.*gf_aux_quad);
    
    if mod(i, 100) == 0
        fprintf('\b\b\b\b\b\b\b\b\b\b\b%05d/%05d', i, numel(t_tau_arr));
    end
    
end
fprintf('\n\n');            




return;




    %function to numerically inverse laplace transform for a given lag and
    %height in the solid
    function gf = laplace_invert(t_tau_curr, gf_mat, theta_vec, r_vec)
        
        
        if t_tau_curr < 2e-10
            t_tau_curr = 2e-10;
        end
        
        %evaluate along integral path segments
        g_bc       = gf_mat{1,1}(t_tau_curr);
        g_cc_prime = gf_mat{1,2}(t_tau_curr);
        g_cb_prime = gf_mat{1,3}(t_tau_curr);
        
                
        %make sure integral is converging, correct for error in which nans
        %appear instead of zeros
        if isempty(find(abs(g_bc) < 1e-15, 1, 'first'))
            fprintf('Error: integrals not converging\n');
        end
        g_bc(isnan(g_bc)) = 0;
        g_cb_prime(isnan(g_cb_prime)) = 0;

     
        %trapezoidal integration
        I_bc       = -trapz(r_vec, g_bc);
        I_cc_prime = trapz(theta_vec, g_cc_prime);
        I_cb_prime = trapz(r_vec, g_cb_prime);

        %sum over the contour to get the inverse laplace transform
        gf = -1/(2*pi*1i)*(I_bc + I_cc_prime + I_cb_prime);
        
        
    end



end












