function q_mat = tsp_heat_flux_tempcorr(T_mat, t_arr, h_arr, alpha_arr, k_arr)
%q_mat = tsp_heat_flux_tempcorr(T_mat, t_arr, h_arr, alpha_arr, k_arr)
%
%Function to compute the heat flux based on TSP measurements. Uses a
%Green's function to describe the evolution of the temperature profile in
%the TSP layer and calls the Regularization Tools package of P.C. Hansen to
%solve the noisy discrete problem. Preprocessing of TSP data (i.e, 
%conversion of intensity to temperature) is assumed to take place
%elsewhere.
%
%This variant of the reconstruction routine uses an iterative correction to
%account for the effect of nonlinear luminophore sensitivity in the
%integrated incident luminosity. This routine uses a model sensitivity
%function that should not be used in actual data processing; the user
%should modify the code as necessary based on the calibration of their TSP
%formulation. A future version of this code will integrate the input of
%general TSP calibrataions more seamlessly.
%
%Inputs:
%       T_mat     - 3D matrix of temperature histories [NROW x NCOL x M]
%       t_arr     - equally spaced data acquisition times [1 x M]
%       h_arr     - thickness of paint/insulator layers [N x 1]
%       alpha_arr - thermal diffusivities [N x 1]
%       k_arr     - thermal conductivities [N x 1]
%
%Outputs: gf_mat   - 3D matrix of heat-flux histories [NROW x NCOL x M]
%
%
%Whalen, Laurence, Marineau, and Ozawa: "A Green’s Function Approach to
%Heat-Flux Estimation from Temperature-Sensitive Paint Measurements"
%
%Please send all comments to whalen@umd.edu
%

fprintf('\n\n********************************************************************\n');
fprintf('USER NOTE: this routine currently uses a model TSP sensitivity.\n');
fprintf('Please modify the code on lines 42--47 to reflect the calibration specific to your TSP formulation.\n');
fprintf('********************************************************************\n');

%%%%%%%%%%%%
% MODEL SENSITIVITY FUNCTION
acal = 0.015;
T0 = 293;
I       = @(T)(          -tanh(acal*(T-T0))+1);
dIdT    = @(T)( -acal   * sech(acal*(T-T0)).^2);
d2IdT2  = @(T)(2*acal^2 * sech(acal*(T-T0)).^2 .* tanh(acal*(T-T0)));
d3IdT3  = @(T)(2*acal^3 * sech(acal*(T-T0)).^4 - 4*acal^3 * sech(acal*(T-T0)).^2 .* tanh(acal*(T-T0)).^2);




%add path to regularization tools
addpath reg_tools

n_img = size(T_mat,3);
n_row = size(T_mat,1);
n_col = size(T_mat,2);
dt = diff(t_arr(1:2));




%%%%%%%%%%%%
% COMPUTE THE TSP GF

fprintf('\nCalculating TSP Green''s function:\n');
t_tau_arr = t_arr - t_arr(1);
gf_tsp = tsp_gf_invert(h_arr, alpha_arr, k_arr, t_tau_arr);
gf_tsp_mat = alpha_arr(1)/k_arr(1) * toeplitz(dt*real(gf_tsp));
gf_tsp_mat = gf_tsp_mat - triu(gf_tsp_mat, 1);






%%%%%%%%%%%%
% COMPUTE HEAT FLUX WITH CORRECTION FOR NONLINEAR SENSITIVITY


fprintf('\nCorrecting for luminophore nonlinear sensitivity:\n');

%first derive the appropriate GFs within the paint layer
n_paint_point = 20;
max_iter = 50;
gf_mat = zeros(n_img, n_img, n_paint_point);
gf     = zeros(n_img, 1);
y_arr_corr = -linspace(0, h_arr(1), n_paint_point);
for i = 1:numel(y_arr_corr)
    n_char = fprintf('Internal GF: %02d/%02d\n', i, (numel(y_arr_corr)));
    gf(:,i) = internal_gf_invert(h_arr, alpha_arr, k_arr, t_tau_arr, y_arr_corr(i));
    gf_mat(:,:,i) = alpha_arr(1)/k_arr(1) * toeplitz(dt*real(gf(:,i)));
    gf_mat(:,:,i) = gf_mat(:,:,i) - triu(gf_mat(:,:,i), 1);
    fprintf(repmat('\b',1,n_char+13));
end


%regularization preparation
svd_lim = 2500;
sub_num = 1000;
lambda_scale = 4;
if n_img > svd_lim
    [U_tsp, S_tsp, V_tsp] = svd(gf_tsp_mat(1:sub_num,1:sub_num));
    Vsp = dct(eye(size(gf_tsp_mat,1)));
    subspace = 50;
else
    [U_tsp, S_tsp, V_tsp] = svd(gf_tsp_mat);
end


q_mat = zeros(n_row, n_col, n_img);
fprintf('\n');
fprintf('                ');
for i = 1:n_row
    fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bRow: %05d/%05d', i, n_row);
    for j = 1:n_col
        dT_bar_corr = 100;
        dT_bar_norm = 1e10;  %just start at arbitrary nonzero number
        T_thresh = 1e-4;   %get convergence
        T_bar_corr = squeeze(T_mat(i,j,:));
        T_bar_corr = movmean(movmean(T_bar_corr, 5), 3);   
        I_est = I(T_bar_corr);
        dT_mat = zeros(n_img, n_paint_point);
        chi = 0.8; nu = 1.15;
        ii = 1;
        
        while norm(dT_bar_corr)/numel(dT_bar_corr) > T_thresh
            
            
            
            %if problem is too large for svd, use iterative routine
            if n_img > svd_lim
                
                %solve the 1000-point subproblem for lambda
                [~, rho, eta, reg_param] = l_curve(U_tsp, diag(S_tsp), T_bar_corr(1:sub_num)-T0, 'Tikh');
                lambda = l_corner(rho, eta, reg_param);
                
                %iterative preconditioned least-squares with scaled lambda
                q_corr = splsqr(gf_tsp_mat, T_bar_corr-T0, lambda_scale*lambda, Vsp(:,1:subspace));
                q_corr = q_corr(:,end);
            else
                
                %svd-based tikhnonov regularization
                [~, rho, eta, reg_param] = l_curve(U_tsp, diag(S_tsp), T_bar_corr-T0, 'Tikh');
                lambda = l_corner(rho, eta, reg_param);
                q_corr = tikhonov(U_tsp, diag(S_tsp), V_tsp, T_bar_corr-T0, lambda_scale*lambda, ones(n_img,1));
                
            end

            
            %calculate resultant temperatures through surface
            for k = 1:numel(y_arr_corr)
                dT_mat(:,k) = squeeze(gf_mat(:,:,k))*q_corr;
            end
            

            %higher-order statistics from virtual temperature profile
            sigma_T2 = std(dT_mat, 0, 2);
            skew_T2 = skewness(dT_mat, 0, 2);
            skew_T2(isnan(skew_T2)) = 0;
            
            %corrected Tbar
            dT_bar_corr = 1./(dIdT(T_bar_corr)) .* (I_est - I(T_bar_corr) - 1/2 * d2IdT2(T_bar_corr) .* sigma_T2.^2 ...
                - 1/6 * d3IdT3(T_bar_corr) .* skew_T2 .*  sigma_T2.^3);
            
            
            %compute new mean temperature history
            T_bar_corr = T_bar_corr + chi * dT_bar_corr;
            
            
            %reduce damping coefficient and compute new dT norm
            if dT_bar_norm > norm(dT_bar_corr)
                chi = chi/nu;
            end
            dT_bar_norm = norm(dT_bar_corr);
            
            
            ii = ii + 1;           
            if ii == max_iter
                break;
            end
            
            
        end
        
        %get a final estimate for the heat flux
        if n_img > svd_lim
            
            %solve the 1000-point subproblem for lambda
            [~, rho, eta, reg_param] = l_curve(U_tsp, diag(S_tsp), T_bar_corr(1:sub_num)-T0, 'Tikh');
            lambda = l_corner(rho, eta, reg_param);
            
            %iterative preconditioned least-squares with scaled lambda
            q_curr =  splsqr(gf_tsp_mat, T_bar_corr-T0, lambda_scale*lambda, Vsp(:,1:subspace));
            q_mat(i,j,:) = q_curr(:,end);
            
        else
            
            %svd-based tikhnonov regularization
            [~, rho, eta, reg_param] = l_curve(U_tsp, diag(S_tsp), T_bar_corr-T0, 'Tikh');
            lambda = l_corner(rho, eta, reg_param);
            q_mat(i,j,:) = tikhonov(U_tsp, diag(S_tsp), V_tsp, T_bar_corr-T0, lambda_scale*lambda, ones(n_img,1));
            
        end
    end
end
fprintf('\n\n');
    

    


return;







end










