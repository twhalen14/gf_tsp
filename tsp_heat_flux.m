function q_mat = tsp_heat_flux(T_mat, t_arr, h_arr, alpha_arr, k_arr, regpar_downsample)
%q_mat = tsp_heat_flux(T_mat, t_arr, h_arr, alpha_arr, k_arr)
%
%Function to compute the heat flux based on TSP measurements. Uses a
%Green's function to describe the evolution of the temperature profile in
%the TSP layer and calls the Regularization Tools package of P.C. Hansen to
%solve the noisy discrete problem. Preprocessing of TSP data (i.e, 
%conversion of intensity to temperature) is assumed to take place
%elsewhere.
%
%Inputs:
%       T_mat     - 3D matrix of temperature histories [NROW x NCOL x M]
%       t_arr     - equally spaced data acquisition times [1 x M]
%       h_arr     - thickness of paint/insulator layers [N x 1]
%       alpha_arr - thermal diffusivities [N x 1]
%       k_arr     - thermal conductivities [N x 1]
%       regpar_downsample - downsampling factor for computing optimized
%                            regularization parameter [1 x 1]
%
%Outputs: gf_mat   - 3D matrix of heat-flux histories [NROW x NCOL x M]
%
%
%Whalen, Laurence, Marineau, and Ozawa: "A Green’s Function Approach to
%Heat-Flux Estimation from Temperature-Sensitive Paint Measurements"
%
%Please send all comments to whalen@umd.edu
%


%add path to regularization tools
addpath reg_tools


n_img = size(T_mat,3);
n_row = size(T_mat,1);
n_col = size(T_mat,2);
dt = diff(t_arr(1:2));





%%%%%%%%%%%%
% COMPUTE THE TSP GF

fprintf('\nCalculating TSP Green''s function:\n');
t_tau_arr = t_arr - t_arr(1);
gf_tsp = tsp_gf_invert(h_arr, alpha_arr, k_arr, t_tau_arr);
gf_tsp_mat = alpha_arr(1)/k_arr(1) * toeplitz(dt*real(gf_tsp));
gf_tsp_mat = gf_tsp_mat - triu(gf_tsp_mat, 1);






%%%%%%%%%%%%
% PRECURSOR CALCULATION OF OPTIMAL REG PARAM ON SPARSE GRID, IF REQUESTED

%check for signal length limitations
svd_lim = 2500;
sub_num = 1000;
lambda_scale = 1;
if n_img > svd_lim
    [U_tsp, S_tsp, V_tsp] = svd(gf_tsp_mat(1:sub_num,1:sub_num));
    Vsp = dct(eye(size(gf_tsp_mat,1)));
    subspace = 50;
else
    [U_tsp, S_tsp, V_tsp] = svd(gf_tsp_mat);
end

%determine whether downsampled computation of reg param was requested
if floor(regpar_downsample) ~= regpar_downsample
    error('Regularization downsampling factor must be integer value.');
    
else
    if regpar_downsample > 1
        fprintf('\nObtaining optimized regularization parameter on sparse grid:\n');
    else 
        fprintf('\nObtaining optimized regularization parameter on standard grid:\n');
    end
    fprintf('                ');

    %assemble the sparse grid
    row_offset = floor(mod(n_row-1, regpar_downsample)/2)+1;
    col_offset = floor(mod(n_col-1, regpar_downsample)/2)+1;
    opt_row = row_offset:regpar_downsample:n_row;
    opt_col = col_offset:regpar_downsample:n_col;
    
    %optimize at each point
    opt_lambda = zeros(numel(opt_row), numel(opt_col));
    for i = 1:numel(opt_row)
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bRow: %05d/%05d', i, numel(opt_row));
        for j = 1:numel(opt_col)
            
            %process current pixel
            T_curr = squeeze(T_mat(opt_row(i),opt_col(j),:));
            T_curr = movmean(movmean(T_curr, 5), 3);  %clean up with limited temporal smoothing
            T_curr = T_curr - T_curr(1);
            
            if n_img > svd_lim
                %solve the 1000-point subproblem for lambda
                [~, rho, eta, reg_param] = l_curve(U_tsp, diag(S_tsp), T_curr(1:sub_num), 'Tikh');            
            else
                [~, rho, eta, reg_param] = l_curve(U_tsp, diag(S_tsp), T_curr, 'Tikh');
            end
            opt_lambda(i,j) = l_corner(rho, eta, reg_param);
            
        end
    end
    
    %check for errant values
    opt_lambda_min = unique(sort(opt_lambda(:)));
    opt_lambda_min = opt_lambda_min(2);
    opt_lambda(opt_lambda==min(reg_param)) = opt_lambda_min;
    
    
    %formulate as anonymous function of gridded interpolant 
    if regpar_downsample > 1
        [opt_col, opt_row] = meshgrid(opt_col, opt_row);
        opt_lambda_interp = griddedInterpolant(opt_row, opt_col, opt_lambda, 'linear');  %bilinear interp to avoid negative internal values
        lambda_func = @(row_q, col_q)opt_lambda_interp(row_q, col_q);
    else
        lambda_func = @(row_q, col_q)opt_lambda(row_q, col_q);
    end
    
       
end
fprintf('\n\n');





%%%%%%%%%%%%
% CALCULATE HEAT FLUX FROM TSP TEMPERATURE

fprintf('\nReconstructing heat flux:\n');
fprintf('                ');
q_mat = zeros(n_row, n_col, n_img);
for i = 1:n_row
    
    fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\bRow: %05d/%05d', i, n_row);

    for j = 1:n_col
        
        %process current pixel
        T_curr = squeeze(T_mat(i,j,:));
        T_curr = movmean(movmean(T_curr, 5), 3);  %clean up with limited temporal smoothing
        T_curr = T_curr - T_curr(1);

        %determine appropriate optimal regularization parameter for this pixel
        lambda = lambda_func(i,j);
        lambda = (abs(lambda) + lambda)/2;  %set negative lambda to zero for extrapolation
        
        %if problem is too large for svd, use iterative routine
        if n_img > svd_lim
            
            %iterative preconditioned least-squares with scaled lambda
            q_curr = splsqr(gf_tsp_mat, T_curr, lambda_scale*lambda, Vsp(:,1:subspace));
            q_mat(i,j,:) = q_curr(:,end);
            
        else
            
            %svd-based tikhnonov regularization
            q_mat(i,j,:) = tikhonov(U_tsp, diag(S_tsp), V_tsp, T_curr, lambda_scale*lambda, ones(n_img,1));

        end
    end
end
fprintf('\n\n');



return;




end

